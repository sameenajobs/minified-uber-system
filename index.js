const threshold = 5; //can be configurable
var riderData = {id:1, name:'Ride 1', location:'latlng', tripID:'generatedID'}; //calling get_riders API will give you all rider related data used during registration
var driversData = {id:1,name:'Driver 1',available:true,location:'latlng',tripID:null};//calling get_drivers API will give you all driver related data used during registration
var tripData = {};

function bookRide(userLocation,riderData) {
    let driver = getNearestDriverInfo(userLocation);
    let tempTrip = {id:121,riderID:1, driverID:1,pickupLatLng:'xxxxxx', dropoffLatLng:'yyyyyy'}
    //generate trip and assign it to rider
    riderData.tripID = tempTrip.id;//random id
    driver.tripID = tempTrip.id;
}

function getNearestDriverInfo(usersLatLng) {
    //iterate thr all the drivers
    for(let i=0; i< driversData.length; i++) {
        // Distance between two location (x1, y1) and (x2, y2) can be
        // calculated using √((x2-x1)2+(y2-y1)2)
        let distance = usersLatLng - driversData[i];
        if(distance < threshold && driversData[i].available) {
            return driversData[i];
        }
    }
}

function registerRider(data) {
    //call POST method and save the data in db
    var riderData = { name:data.name, location:data.latlng, tripID:'null'};
    // after successful rider creation, id will be generated
}

function registerDriver(data) {
    var driverData = {name:data.name, available:data.status, location:data.latlng, tripID:null};
    // after successful driver creation, id will be generated
}
